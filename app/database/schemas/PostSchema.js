import Constant from '../../utils/Constant';

export default PostSchema = {
  'name': Constant.KEY_POST,
  'properties': {
    'id': 'int',
    'intro': 'string',
    'title': 'string',
    'created_on': 'date',
    'tags': 'string',
    'categories': 'string',
    'featured_img': 'string',
    'content': 'string',
    'liked': 'bool'
  }
}