import Constant from '../../utils/Constant';

export default CategorySchema = {
  'name': Constant.KEY_CATEGORY,
  'properties': {
    'name': 'string',
    'id': 'int',
    'slug': 'string',
    'chosen': 'bool'
  }
}