import React, { Component } from 'react';
import { ScrollView, Text, View, ActivityIndicator, Alert } from 'react-native';
import { FormLabel, FormInput, Button, FormValidationMessage } from 'react-native-elements';

import API from '../../api/ApiRequest';

export default class FragSubscribe extends Component {

  constructor(props) {
    super(props);
    this.state = {
      error: {
        email: '',
        name: ''
      },
      input: {
        email: '',
        name: ''
      },
      busy: false
    };
  }

  async onSubscribePressed() {
    if (this.state.input.name.trim().length == 0) {
      return this.setState({
        error: {
          name: 'Opps, you forgot to fill the name field.'
        }
      });
    }
    if (!this.state.input.email || this.state.input.email.trim().length == 0) {
      return this.setState({
        error: {
          email: 'Opps, you forgot to fill the email field.'
        }
      });
    }
    if (this.state.input.email.indexOf('@') < 2 || this.state.input.email.indexOf('.') < 5) {
      return this.setState({
        error: {
          email: 'Please make sure the email is valid.'
        }
      });
    }
    this.setState({ busy: true });
    const response = await API.newSubscriber(this.state.input.name, this.state.input.email);
    if (response && response.status && response.status == 'success') {
      Alert.alert('Congrats!', 'You are not subscribed to newsletters.');
    } else {
      Alert.alert('Oops!', 'Sorry, we couldnot subscribe you to the list.');
    }
    this.setState({ busy: false });
  }

  onInput(type, value) {
    if (type == 'email') {
      const lastName = this.state.input.name;
      this.setState({
        input: {
          email: value,
          name: lastName
        },
        error: {
          email: ''
        }
      });
    } else {
      const lastEmail = this.state.input.email;
      this.setState({
        input: {
          name: value,
          email: lastEmail
        },
        error: {
          name: ''
        }
      });
    }
  }

  getLoading() {
    return <ActivityIndicator size='small' animating color='#008A7C' />;
  }

  getSubmit() {
    return <Button buttonStyle={{ backgroundColor: '#0090F7' }} iconLeft icon={{ name: 'email' }} onPress={this.onSubscribePressed.bind(this)} style={{ marginTop: 16, marginBottom: 16 }} title='Subscribe' />;
  }


  render() {
    const control = this.state.busy ? this.getLoading() : this.getSubmit();
    return (
      <ScrollView>
        <View style={{ padding: 16 }}>
          <Text style={{ marginBottom: 12, color: '#555', fontWeight: 'bold', fontSize: 32, textAlign: 'center' }}>Stay tuned!</Text>
          <Text style={{ color: '#aaa', textAlign: 'center' }}>Subscribe with your email and we will deliver updates directly to your inbox.</Text>
        </View>
        <View style={{ backgroundColor: '#fff', paddingBottom: 16 }}>
          <FormLabel>Name</FormLabel>
          <FormInput onChangeText={ this.onInput.bind(this, 'name') } />
          <FormValidationMessage>{this.state.error.name}</FormValidationMessage>
          <FormLabel>Email</FormLabel>
          <FormInput onChangeText={ this.onInput.bind(this, 'email') } />
          <FormValidationMessage>{this.state.error.email}</FormValidationMessage>
          { control }
        </View>
      </ScrollView>
    );
  } 

}