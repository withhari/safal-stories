import React, { Component } from 'react';
import { ActivityIndicator, ScrollView, View, AppRegistry, Text, Alert } from 'react-native';
import { Button } from 'react-native-elements';

import RoundedButton from '../../components/RoundedButton';
import API from '../../api/ApiRequest';
import Session from '../../utils/Session';
import Store from '../../utils/Store';
import Helper from '../../utils/Helper';
import DB from '../../database/DB';
import Constant from '../../utils/Constant';

export default class FragChooseCategory extends Component {

  constructor(props) {
    super(props);
    this.state = {
      chosen: [],
      showChoose: true,
      used: this.props.existing ? this.props.categories.filter(cat => cat.chosen).map(cat => cat.id) : []
    };
  }

  onPress(category, active) {
    if (active) {
      this.state.chosen.push(category.id);
    } else {
      const filtered = this.state.chosen.filter(id => id != category.id);
      const remove = this.state.used.filter(id => id != category.id);
      this.setState({
        chosen: filtered,
        used: remove
      });
    }
  }

  onNext() {
    if (this.props.existing) {
      this.startFetching();
      return;
    }

    if (this.state.chosen.length < 3) {
      Alert.alert('Insufficient category', 'Please, choose at least three categories.');
    } else {
      this.startFetching();
    }
  }

  startFetching() {
    const all = this.state.chosen.concat(this.props.existing ? [] : Constant.EXCLUDE_CATEGORY);
    let tmp = '';
    if (this.props.existing) {
      tmp = '&exlcude='+ this.state.used.join(',');
    }
    const params = 'categories=' + all.join(',') + tmp;
    this.setState({ showChoose: false });
    this.saveChoosen();
    this.fetchPost(1, params);
  }

  saveChoosen() {
    this.state.chosen.forEach(categoryId => {
      DB.updateCategory(categoryId, true);
    });
  }

  async fetchPost(page, params) {
    const posts = await API.getPosts(page, params);
    for(index in posts) {
      let post = posts[index];
      post.featured_media = post.featured_media+'';
      this.savePost(post);
    }
    if (posts.length == 100) {
      this.fetchPost(++page, params);
    } else {
      Helper.setFirstRun(Constant.KEY_NOT_FIRST_RUN);
      this.props.navigation.navigate(Constant.NAV_MAIN);
    }
  }

  savePost(post) {
    requestAnimationFrame(() => {
      if (!post || !post.title) return;
      DB.addPost(post.id, 
             post.title.rendered,
             post.excerpt.rendered, 
             post.content.rendered,
             post.categories.join(','), 
             post.featured_media,
             post.tags.join(','),
             new Date(post.date));
    });
  }

  render() {
    if (this.state.showChoose) {
      return this.getChooseView();
    } else {
      return this.getProgressView();
    }
  }

  getProgressView() {
    if (this.props.existing) {
      return  <ActivityIndicator style={{ marginTop: 128 }} color='blue' size='large' />;
    }
    return (
      <View style={{ flex: 1, flexDirection: 'column', alignContent: 'center', justifyContent:'center', backgroundColor: '#fff', paddingLeft: 16, paddingRight: 16, paddingTop: 32, paddingBottom: 32 }}>
        <Text style={{ textAlign: 'center', color: '#77f', fontWeight: 'bold', fontSize: 18 }}>Bear with us!</Text>
        <Text style={{ textAlign: 'center', marginTop: 12, marginBottom: 12, color: '#9f9f9f', fontSize: 14 }}>We are building up feed for you.</Text>
        <Text style={{ textAlign: 'center', color: '#9f9f9f', fontSize: 14, fontWeight: 'bold' }}>This is only for the first run, we promise!</Text>
        <ActivityIndicator style={{ marginTop: 16 }} color='blue' size='large' />
      </View>
    );
  }


  getChooseView() {
    const categories = this.props.categories.filter(category => Constant.EXCLUDE_CATEGORY.indexOf(category.id) === -1).map(category => {
      return <RoundedButton active={category.chosen || false} key={category.id} onPress={this.onPress.bind(this, category)} title={category.name} />
    });
    return (
      <View style={{ flex: 1, backgroundColor: '#fff', paddingLeft: 16, paddingRight: 16, paddingTop: 32, paddingBottom: 32 }}>
        <Text style={{ textAlign: 'center', color: '#77f', fontWeight: 'bold', fontSize: 18 }}>{this.props.existing ? 'Update' : 'Choose'} topics!</Text>
        { !this.props.existing && <Text style={{ marginTop: 12, color: '#9f9f9f', fontSize: 14 }}>Please, choose at least 3 categories to continue.</Text>}
        { !this.props.existing && <Text style={{ color: '#9f9f9f', fontSize: 14, fontStyle: 'italic' }}>You can change them later.</Text>}
        <View style={{
          flexDirection: 'row',
          flexWrap: 'wrap',
          paddingTop: 32,
          paddingBottom: 32,
          alignContent: 'space-between'
        }}>
          { categories }
        </View>
        <Button style={{ margin: 0 }} title={ this.props.existing ? 'Update' : 'Next' } onPress={this.onNext.bind(this)} />
      </View>
    );
  }
}