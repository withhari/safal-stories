import React, { Component } from 'react';
import { ActivityIndicator, Share, View, Text, ScrollView, StyleSheet, Image, Dimensions, TouchableOpacity } from 'react-native';
import HTMLView from 'react-native-htmlview';
import { Icon } from 'react-native-elements';

import DB from '../database/DB';
import Constant from '../utils/Constant';
import Session from '../utils/Session';
import API from '../api/ApiRequest';

export default class Post extends Component {

  static navigationOptions = {
    title: '',
    header: null
  };

  constructor(props) {
    super(props);
    const { post } = this.props.navigation.state.params;
    this.state = {
      liked: post.liked
    }
    this.fetchMedia(post);
  }

  async fetchMedia(post) {
    if (post.featured_img.length > 8) {
      return;
    }
    try {
      const media = await API.getMedia(post.featured_img);
      DB.setImage(post, media, (p => {
        this.props.navigation.state.params.post = p;
        this.setState({ updated: true });
      }).bind(this));
    } catch (ignore) {

    }
  }

  strip_tags (input, allowed) { 
    allowed = (((allowed || '') + '').toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join('')
    var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi
    var capt = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi
    return input.replace(capt, '').replace(tags, function ($0, $1) {
      return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
    });
  }

  trim(txt) {
    return txt.split("\n").map(line => line.trim()).filter(line => line.length !== 0).join('');
  }

  goBack() {
    this.props.navigation.goBack();
  }

  renderNode(node, index, siblings, parent, defaultRenderer) {
    if (node.name == 'blockquote') {
      let quote = null
      let tmpNode = node.children[0];
      while (tmpNode.type !== 'text') {
        tmpNode = tmpNode.children[0];
      }
      quote = tmpNode.data;
      return <View style={styles.quoteContainer} key={index}><Text style={styles.quote}>{quote}</Text></View>;
    }
    if (node.name == 'img') {
      const width = Dimensions.get('window').width - 24;
      const height = 143/300 * width;
      return <Image key={node.attribs['data-attachment-id']} resizeMethod='scale' resizeMode='contain' source={{ uri: node.attribs['data-medium-file'], height: height, flex: 1, width: width }} />;
    }
  }

  buildLabel(categories) {
    const all = Session.get(Constant.KEY_CATEGORY, []).value;
    // remove featured label
    const colors = [ '#77a', '#a46', '#8c8' ];
    const result = categories.split(',').map(id => {
      const color = colors[id % 3];
      return <Text style={{ fontSize: 12, textAlign: 'center', marginRight: 2, borderRadius: 2, borderWidth: 1, borderColor: color, paddingLeft: 2 , paddingRight: 2 }} key={id}>{ all.filter(cat => cat.id == id)[0].name }</Text>;
    });
    return result;
  }

  onLike(post) {
    const wasLiked = post.liked;
    this.setState({
      liked: !wasLiked
    });
    setTimeout(() => DB.updatePost(post.id, !wasLiked), 1);
  }

  async onShare(post) {
    const intro = this.strip_tags(post.intro);
    const shared = await Share.share({ title: post.title, message: intro + "\nMore at: http://safalstories.com?p="+post.id }, {
      dialogTitle: 'Share story'
    });
  }

  render() {
    const { post } = this.props.navigation.state.params;
    const icon = this.state.liked ? 'heart' : 'heart-o';
    const imgOrLoading = post.featured_img.length > 5 ? <Image style={{ marginBottom: 8, marginTop: 8 }} source={{ uri: post.featured_img, height: 130 }} resizeMethod='resize' resizeMode='cover' />
                          : <ActivityIndicator size='large' color='blue' style={{ margin: 32, alignSelf: 'center' }} />
    
    return (
      <ScrollView style={{ backgroundColor: '#fff' }}>
        <View elevation={2} style={styles.header}>
          <TouchableOpacity activeOpacity={.50} onPress={this.goBack.bind(this)}>
            <Icon style={{ padding: 6 }} size={14} name='arrow-left' type='simple-line-icon' color='#000' />
          </TouchableOpacity>
          <Text style={styles.title}>{post.title.trim()}</Text>
          <View style={{ flex: .2, flexDirection: 'row', justifyContent: 'flex-end' }}>
            <Icon style={{ padding: 6, marginRight: 4 }} size={14} onPress={this.onLike.bind(this, post)} name={icon} type='font-awesome' color='red' />
            <Icon style={{ padding: 6 }} size={14} onPress={this.onShare.bind(this, post)} name='share' type='font-awesome' color='blue' />
          </View>
        </View>
        { imgOrLoading }
        <View style={styles.meta}>
          <View style={{ flex: .7, flexDirection: 'row', flexWrap: 'wrap' }}>{this.buildLabel(post.categories)}</View>
          <Text style={{ flex: .3, fontSize: 10, textAlign: 'right' }}>{post.created_on.toDateString()}</Text>
        </View>
        <HTMLView style={styles.content} renderNode={this.renderNode} stylesheet={styles} value={this.trim(post.content)} />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#fff',
    flex: 1,
    flexDirection: 'row',
    paddingTop: 12,
    paddingBottom: 8,
    paddingLeft: 10,
    paddingRight: 16,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,.1)'
  },
  title: {
    flex: .6,
    fontSize: 16,
    paddingTop: 1,
    paddingLeft: 8,
    color: '#008A7C'
  },
  meta: {
    flex: 1,
    flexDirection: 'row',
    paddingLeft: 12,
    paddingRight: 12,
    paddingTop: 4,
    paddingBottom: 4,
  },
  quoteContainer: {
    padding: 4,
    backgroundColor: '#FCFCFC',
    borderLeftWidth: 4,
    borderLeftColor: '#8962af',
    marginTop: 8,
    marginBottom: 8
  },
  quote: {
    color: '#009688',
    textAlign: 'center',
    fontWeight: '500',
    fontStyle: 'italic'
  },
  content: {
    paddingLeft: 12,
    paddingRight: 12,
    paddingTop: 8,
    paddingBottom: 8,
    marginTop: 8,
    borderTopWidth: 1,
    borderTopColor: 'rgba(0,0,0,.1)',
    backgroundColor: '#fff'
  }
});