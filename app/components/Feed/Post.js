import React, { Component } from 'react';
import HTMLView from 'react-native-htmlview';
import { Text, View, StyleSheet, TouchableOpacity, Image, Share } from 'react-native';
import { Card, Header, Icon } from 'react-native-elements';

import DB from '../../database/DB';
import Constant from '../../utils/Constant';
import Session from '../../utils/Session';

export default class Post extends Component {

  constructor(props) {
    super(props);
  }

  strip_tags (input, allowed) { 
    allowed = (((allowed || '') + '').toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join('')
    var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi
    var capt = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi
    return input.replace(capt, '').replace(tags, function ($0, $1) {
      return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
    });
  }

  onItemPress(post) {
    if (this.props.nav) {
      this.props.nav.navigate(Constant.NAV_SHOW_POST, { post: post });
    }
  }

  render() {
    const { post } = this.props;
    let intro = decodeURI(post.intro.trim()).split(' ');
    intro = intro.slice(0, intro.length > 15 ? 15 : intro.length).join(' ') + '...';
    return (
      <TouchableOpacity onPress={this.onItemPress.bind(this, post)} activeOpacity={.8} style={styles.postContainer}>
        <Text style={styles.title}>{post.title.trim()}</Text>
        {/* <Image source={{ uri: post.featured_img, height: 130 }} resizeMethod='resize' resizeMode='cover' /> */}
        <HTMLView stylesheet={HTMLStyles} value={intro} />
      </TouchableOpacity>
    );
  }

}

const HTMLStyles = StyleSheet.create({
  p: {
    fontSize: 13
  }
});

const styles = StyleSheet.create({
  postContainer: {
    backgroundColor: '#fff',
    marginBottom: 8,
    paddingLeft: 16,
    paddingRight: 16,
    paddingTop: 8,
    paddingBottom: 8,
  },
  content: {

  },
  title: {
    fontSize: 15,
    fontWeight: 'bold',
    paddingBottom: 4,
    paddingTop: 4,
    color: '#008A7C'
  },
  footer: { 
    flex: 1, 
    flexDirection: 'row',
    borderTopWidth: 1,
    paddingTop: 8,
    marginTop: 8,
    borderTopColor: 'rgba(0,0,0,.1)'
  }
});