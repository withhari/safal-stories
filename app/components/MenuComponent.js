import React, { Component } from 'react';
import { List, ListItem } from 'react-native-elements';
import { StyleSheet } from 'react-native';

import FragFeed from '../containers/fragments/FragFeed';
import FragGOI from '../containers/fragments/FragGOI';
import FragSubscribe from '../containers/fragments/FragSubscribe';
import FragAbout from '../containers/fragments/FragAbout';

export default class MenuComponent extends Component {

  render() {
    const current = this.props.currentIndex;
    const Menu = [
      { screen: FragFeed, title: 'Home', icon: 'home', color: '#B8B6C0', arg: 'default' },
      { screen: FragFeed, title: 'Loved Stories', icon: 'heart', color: '#B8B6C0', arg: 'loved' },
      { screen: FragFeed, title: 'Safal Story', icon: 'trophy', color: '#B8B6C0', arg: 'story' },
      { screen: FragFeed, title: 'Safal Tips', icon: 'book-open', color: '#B8B6C0', arg: 'tips' },
      { screen: FragGOI, title: 'Gallery of Inspiration', icon: 'folder', color: '#B8B6C0' },
      { screen: FragSubscribe, title: 'Subscribe', icon: 'envelope', color: '#B8B6C0' },
      { screen: FragAbout, title: 'About us', icon: 'info', color: '#B8B6C0' },
    ].map((item, index) => {
      const stls = [styles.listItem];
      const titleStls = [styles.title];
      if (index === current) {
        stls.push(styles.listItemSelected);
        titleStls.push(styles.titleSelected);
        item.color = '#FFFFFF';
      }
      return (
         <ListItem titleStyle={titleStls} 
                   onPress={this.props._onItemPressed.bind(this, index, item)} 
                   style={stls} hideChevron={true} 
                   leftIcon={{color: item.color, name: item.icon, type: 'simple-line-icon', size: 15}}
                   key={index} title={item.title} />
      );
    });
    return (
      <List style={styles.menu}>{Menu}</List>
    );  
  }
}

const styles = StyleSheet.create({
  menu: {
    paddingTop: 120,
    flex: 1,
    flexDirection: 'column',
    margin: 0,
    borderWidth: 0,
    backgroundColor:'#2C2843',
  },
  listItem: {
    backgroundColor:'#2C2843',
    paddingTop: 13,
    paddingBottom: 13,
    borderLeftWidth: 4,
    borderLeftColor: 'transparent'
  },
  listItemSelected: {
    borderLeftColor: '#228DFF',
    backgroundColor: '#201A34'
  },
  title: { 
    color: '#afafaf',
    fontSize: 15
  },
  titleSelected: {
    color: '#FFFFFF',
  }
});