import React, { Component } from 'react';
import { AppRegistry, Text } from 'react-native';

import App from './app/App';

AppRegistry.registerComponent('safalstories', () => App);